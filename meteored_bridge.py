import json
import logging
import os
import urllib.parse
from xml.dom import minidom

from request_cache import DailyRequestCache, DailyTime

REQUEST_CACHE = DailyRequestCache(extra_reset_times=(DailyTime(hour=4),))

from programaker_bridge import (  # Needed for argument definition; Import bridge functionality
    BlockContext, CallbackBlockArgument, CallbackSequenceBlockArgument,
    ProgramakerBridge, VariableBlockArgument)

bridge = ProgramakerBridge(
    name="TheWeather",
    endpoint=os.environ["BRIDGE_ENDPOINT"],
    token=os.getenv("BRIDGE_AUTH_TOKEN", None),
    is_public=True,
    icon=open(os.path.join(os.path.dirname(__file__), "meteored-logo.png"), "rb"),
)

AFFILIATE_ID = os.environ["AFFILIATE_ID"]
ROOT_API = "https://api.theweather.com/index.php"


# API's xml parsing
def get_xml_text(base_node):
    todo = [base_node]
    result = []

    while len(todo) > 0:
        node = todo.pop()

        if node.nodeType == node.TEXT_NODE:
            result.append(node.data)
        elif node.nodeType == node.ELEMENT_NODE:
            todo.extend(node.childNodes)

    return "".join(result)


def get_data_id_from_url(url):
    query = urllib.parse.urlparse(url).query
    props = urllib.parse.parse_qs(query)
    del props["api_lang"]

    keys = list(props.keys())
    assert len(keys) == 1

    return "{}={}".format(keys[0], props[keys[0]][0])


def get_items_in(selector):
    if selector.startswith("localidad"):
        # Can't zoom more
        return [{"name": "default", "id": selector}]

    response = REQUEST_CACHE.request(
        "{}?{}&affiliate_id={}".format(ROOT_API, selector, AFFILIATE_ID)
    )
    doc = minidom.parseString(response)
    items = []
    for data in doc.getElementsByTagName("data"):
        name = get_xml_text(data.getElementsByTagName("name")[0])
        url = get_xml_text(data.getElementsByTagName("url")[0])
        data_id = get_data_id_from_url(url)

        items.append({"name": name, "id": data_id})

    return items


# API requests for zooming
@bridge.callback
def get_continents(_extra_data):
    return get_items_in("continente=0")


@bridge.callback
def get_countries(continent_id, _extra_data):
    return get_items_in(continent_id)


@bridge.callback
def get_regions(country_id, _extra_data):
    return get_items_in(country_id)


@bridge.callback
def get_locations(division_id, _extra_data):
    return get_items_in(division_id)


def get_data_in_location(location_id, _extra_data):
    response = REQUEST_CACHE.request(
        "{}?{}&v=3.0&affiliate_id={}".format(ROOT_API, location_id, AFFILIATE_ID)
    )
    doc = json.loads(response)
    return doc


@bridge.callback
def get_items(_extra_data):
    return [
        {"name": "max temperature (in C)", "id": "tempmax"},
        {"name": "min temperature (in C)", "id": "tempmin"},
        {"name": "wind speed", "id": "wind.speed"},
        {"name": "humidity", "id": "humidity"},
        {"name": "weather description", "id": "symbol_description"},
    ]


@bridge.callback
def get_day_offsets(_extra_data):
    return [
        {"id": "1", "name": "the current day"},
        {"id": "2", "name": "the next day"},
        {"id": "3", "name": "2 days from the current day"},
        {"id": "4", "name": "3 days from the current day"},
        {"id": "5", "name": "4 days from the current day"},
    ]


@bridge.getter(
    id="get_item_in_day_from_location",
    message="Get %1 %2 in %3",
    arguments=[
        CallbackBlockArgument(str, get_items),
        CallbackBlockArgument(str, get_day_offsets),
        CallbackSequenceBlockArgument(
            str, [get_continents, get_countries, get_regions, get_locations]
        ),
    ],
    block_result_type=str,
)
def get_max_prediction(item, day_offset, place_code, extra_data):
    # Getter logic
    logging.info("GET max-prediction on {}".format(place_code))
    data = get_data_in_location(place_code, extra_data)["day"][str(day_offset)]
    for section in item.split("."):
        data = data[section]
    return data


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
    logging.getLogger().setLevel(logging.INFO)

    logging.info("Starting bridge")
    bridge.run()
